Instrucciones para la ejecucion del supermenu:

a) ip addr show:
Nada. Deberia andar perfectamente.

b) route:
Hay que instalar net-tools:
$sudo apt-get install net-tools

c) ping:
Deberia andar perfectamente
instalar nmap

d) ssh:  (Secure Shell)
1-Hay que configurar el servidor en una PC
	$ sudo apt-get install openssh-server
	$ sudo /etc/init.d/ssh start
	...
	$ sudo /etc/init.c/ssh stop  ---> cuando terminaste de usarlo

2- Hay que establece la conexion en otra compu

	$sudo ssh -p 22 -X usuario@ip
	
	

e) scp-> Secure Copy

1- Tiene que estar configurado el servidor (ver d)
2- Del servidor a mi computadora:
	$ scp martindome@192.168.10.18:ungs/sor/tp2/ejercicio1/red.c /home/juanperez/MiCarpeta 
	$ scp -r martindome@192.168.10.18:ungs/sor/tp2/ejercicio1 miCarpeta --> todo el directorio
3- De mi pc al servidor:
	$ scp "red.c" martindome@192.168.10.18:Escritorio                   --> el archivo
	$ scp /home/juanperez/tp/*.sh martindome@192.168.10.18:ungs/tp/     --> todos los archivos con ese nombre
	$ scp -r /home/juanperez/miCarpeta martindome@192.168.10.18:Escritorio/tp --> todo el directorio


