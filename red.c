#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

char* duplicar_string_n(const char *str, size_t i) {

	char* str_nuevo = (char*) malloc (sizeof(char*) * (strlen(str)+1));
	if (!str_nuevo)
		return NULL;
	strncpy(str_nuevo, str, i);
	str_nuevo[i] = '\0';
	return str_nuevo;
}

char** split(const char* str, char sep) {
	
	if (sep == '\0')
		return NULL;
	int i = 0, cont_sep = 0;
	while(str[i] != '\0') {
		if (str[i] == sep) 
			cont_sep++;
		i++;
	}
	char **strv = (char**) malloc ( sizeof(char*) * (cont_sep + 2));
	if (!strv)
		return NULL;
	int j = 0, cont = 0, anterior = 0, cant_letras = 0;
	while(cont <= cont_sep) {
		cant_letras++;
		if (str[j] == sep || str[j] == '\0') {
			strv[cont] = duplicar_string_n(str+anterior, cant_letras-1);
			anterior = j+1;
			cant_letras = 0;
			cont++;
		}
		j++;
	}				
	strv[cont_sep + 1] = '\0'; 
	return strv; 	
}

void free_strv(char *strv[]) {

	int i = 0;
	while(strv[i] != NULL) {
		free(strv[i]);
		i ++;
	}
	free(strv);
}

void mostrar_strv(char** strv) {
	
	int i = 0;
	printf("(");
	while (strv[i]) {
		printf("%s-",strv[i]);
		i++;
	}
	printf("NULL)\n");
}

int potencia(int base, int exponente){
	int res;
       if (exponente == 0)
        res = 1;
       else
        res = base * potencia(base, exponente-1);
       return res;
}

int main(int argc, char *argv[]){

	char* str = argv[1];

	char** strv = split(str, '/');
	int mascara = atoi(strv[1]);  //atoi: array to int, itoa: int to array

	int i = 0;
	int cont  = 8;
	int num = 0;
	char octeto_0[4], octeto_1[4], octeto_2[4], octeto_3[4];
	char* octetos_mascara[4] = {octeto_0, octeto_1, octeto_2, octeto_3};
	while (mascara>0){
		while (cont > 0 && mascara > 0){
			num = num + potencia(2, cont-1);
			mascara--;
			cont--;
		}
		sprintf(octetos_mascara[i], "%d", num);
		//octetos_mascara[i][3] = '\0';
		i++;
		num = 0;
		cont = 8;
	}
	while (i < 4){
		num = 0;
		sprintf(octetos_mascara[i], "%d", num);
		//octetos_mascara[i][3] = '\0';
		i++;
	}
	char** octetos_ip = split(strv[0], '.');

	printf("su red es:\n");
	for (int i = 0; i < 4; i++){
		int ip = atoi(octetos_ip[i]);
		int mask = atoi(octetos_mascara[i]);
		int c = 0; 
		c = mask & ip;
		if(i== 3)
			printf("%d",c);
		else
			printf("%d.", c);         
	}

	free_strv(strv);
	free_strv(octetos_ip);

}
